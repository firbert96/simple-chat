const socket = io.connect()
const message = document.getElementById('message')
const userName = document.getElementById('userName')
const sendBtn = document.getElementById('send')
const messages = document.getElementById('messages')
const typing = document.getElementById('typing')
const upload = document.getElementById('upload')

function addElement(parent,elementTag, elementId) {
    let newElement = document.createElement(elementTag);
    newElement.setAttribute('id', elementId);
    parent.appendChild(newElement);
}

function removeElement(elementId) {
    let element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}

sendBtn.addEventListener('click',function(){
    socket.emit('chat',{
        message: message.value,
        userName: userName.value
    })
})

socket.on('chat',function(data){
    messages.innerHTML += `<p><strong>${data.userName}: </strong>${data.message}</p>` 
    typing.innerHTML = ""
})

message.addEventListener('keypress',function(){
    socket.emit('typing',userName.value)
})

socket.on('typing',function(data){
    typing.innerHTML = `<p><em>${data} is typing</em></p>`
})

upload.addEventListener('click',function(){
    let file = document.getElementById('filename').files[0]
    console.log(file)
    if(file.size>=60*1000*1000){
        alert('Cannot upload file greater than 60 mb')
        return
    }
    socket.emit('attachment',{
        userName: userName.value,
        fileName: file.name,
        fileType: file.type,
        file
    })
    addElement(messages,'div','loader')
})

socket.on('image',function(data){
    let result = data.file
    console.log(result)
    messages.innerHTML += `<p><strong>${data.userName}: </strong></p><img src=${result} width="20%" height="20%">`  
    typing.innerHTML = ''
    removeElement('loader')
})

socket.on('video',function(data){
    let result = data.file
    console.log(result)
    messages.innerHTML += `<p><strong>${data.userName}: </strong></p><video width=20% height="20%" controls><source src=${result} type="video/mp4"></video>`
    typing.innerHTML = ''
    removeElement('loader')
})

socket.on('audio',function(data){
    let result = data.file
    console.log(result)
    messages.innerHTML += `<p><strong>${data.userName}: </strong></p><audio controls><source src=${result} type="audio/mp3"></audio>`
    typing.innerHTML = ''
    removeElement('loader')
})

socket.on('raw',function(data){
    let result = data.file
    console.log(result)
    messages.innerHTML += `<p><strong>${data.userName}: </strong></p><label>${data.fileName}</label><a href=${result}>Download!</a>`
    typing.innerHTML = ''
    removeElement('loader')
})